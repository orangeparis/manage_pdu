# build the docker image
image_name="manage_pdu:local"
docker build  --force-rm --no-cache -t ${image_name} --network host .

# archive image
docker save ${image_name} | gzip > ${image_name}.tgz
#docker tag ${image_name}:latest ${image_name}:local

