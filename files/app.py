#!/usr/bin/env python
# -*- coding: utf-8 -*-

import asyncio
import signal
import manage_pdu as pdu
import logging


async def shutdown(signal, loop):
    """Cleanup tasks tied to the service's shutdown."""
    logging.info(f"Received exit signal {signal.name}...")
    logging.info("Closing ...")
    tasks = [t for t in asyncio.all_tasks() if t is not
             asyncio.current_task()]

    [task.cancel() for task in tasks]

    logging.info(f"Cancelling {len(tasks)} outstanding tasks")
    await asyncio.gather(*tasks)
    logging.info(f"Flushing metrics")
    loop.stop()


def main():
    loop = asyncio.get_event_loop()
    # May want to catch other signals too
    signals = (signal.SIGHUP, signal.SIGTERM, signal.SIGINT)
    for s in signals:
        loop.add_signal_handler(
            s, lambda s=s: asyncio.create_task(shutdown(s, loop)))

    try:
        # loop.create_task(pdu.run(loop))
        # loop.run_forever()
        loop.run_until_complete(pdu.run(loop))
    except asyncio.CancelledError:
        logging.info("Process interrupted")
    finally:
        loop.close()
        logging.info("Successfully shutdown the pdu service.")


if __name__ == "__main__":

    # load csv file
#    pdu.csv_load()

    # run async loop
    main()

    # loop = asyncio.get_event_loop()
    # loop.run_until_complete(pdu.run(loop))
    # loop.close()
    # logging.info("Successfully shutdown the pdu service.")


