*** Settings ***
Library    Lib_manage_pdu.py
Library    Lib_DYT

*** Variable ***

*** Test Cases ***
031a pdu off then off
    [Tags]    E2E    NominalCase
    log  ${\n}---------- PDU PDU-M3-P2 on then off for attached Livebox Livebox-5EFC     console=yes
    ${s}   ${m}   run keyword and ignore error      manage_pdu       192.168.100.60 	2 "off"	
    run keyword if      "${s}" == "PASS"   Lib_DYT.Add Result       metric=Connection     status=PASS     target=Telnet
    run keyword if      "${s}" == "FAIL"   Lib_DYT.Add Result       metric=Connection     status=FAIL     target=Telnet   comment=${m}

    ${s}   ${m}   run keyword and ignore error      manage_pdu       192.168.100.60 	2 "on"	
    run keyword if      "${s}" == "PASS"   Lib_DYT.Add Result       metric=Connection     status=PASS     target=Telnet
    run keyword if      "${s}" == "FAIL"   Lib_DYT.Add Result       metric=Connection     status=FAIL     target=Telnet   comment=${m}
