*** Settings ***
Library    Lib_manage_pdu.py
Library    Lib_DYT

*** Variable ***

*** Test Cases ***
031a pdu off then off
    [Tags]    E2E    NominalCase
    log  ${\n}---------- PDU PDU-M2-P6 on then off for attached Livebox Livebox-5D34     console=yes
    ${s}   ${m}   run keyword and ignore error      manage_pdu|"http://10.194.67.234:8080"|"192.168.100.55"|"1" "off"
    run keyword if      "${s}" == "PASS"   Lib_DYT.Add Result       metric=Connection     status=PASS     target=Telnet
    run keyword if      "${s}" == "FAIL"   Lib_DYT.Add Result       metric=Connection     status=FAIL     target=Telnet   comment=${m}

    ${s}   ${m}   run keyword and ignore error      manage_pdu|http://10.194.67.234:8080|192.168.100.55|1|"on"
    run keyword if      "${s}" == "PASS"   Lib_DYT.Add Result       metric=Connection     status=PASS     target=Telnet
    run keyword if      "${s}" == "FAIL"   Lib_DYT.Add Result       metric=Connection     status=FAIL     target=Telnet   comment=${m}
