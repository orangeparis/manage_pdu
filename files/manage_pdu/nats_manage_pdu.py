# -*- coding: utf-8 -*-
import asyncio
from nats.aio.client import Client as NATS
from nats.aio.errors import ErrConnectionClosed, ErrTimeout, ErrNoServers
import csv
import json

from .helper import access_response, response, error_response, parameters, gitoun

import logging
#from logging.handlers import RotatingFileHandler
import argparse
#from git import repo
import os
from .telnet_pdu import pdu_ctrl


logger = logging.getLogger()

parser = argparse.ArgumentParser()
parser.add_argument("-n","--nats", help="nats url server nats://127.0.0.1:4222")

args = parser.parse_args()
if args.nats:
    NATS_SERVER=args.nats
else:
    NATS_SERVER="nats://127.0.0.1:4222"

async def run(loop):
    newmsg = {
       "result": {
          "get": True,
          "call": "on,off,restart,state,ping,options"
       }
    }
    logger.info("Connexion au bus nats")
    nc = NATS()

    await nc.connect(NATS_SERVER, loop=loop)

    async def access_handler(msg):
        await nc.publish(msg.reply, json.dumps(newmsg).encode())

    async def pdu_handler(msg):
        logger.info("subject: {subj}".format(subj=msg.subject))
        logger.info("reply: {repl}".format(repl=msg.reply))
        logger.info("data: {data}".format(data=json.loads(msg.data.decode())))
        data = json.loads(msg.data.decode())
        logger.info("data du message = {}".format(data))
        response = {
            "result": {"message": "syntax: call.manage_pdu.on/off/state/restart"}
        }
        try:
            #params = json.loads(data['params'])
            logger.info("Avant Erreur")
            params = data['params']
#            logger.info("Parameters loaded: {params}".format(params=params))
            print("parameters loaded: {}".format(params))
            if msg.subject.lower() == "call.manage_pdu.on":
                logger.info("handled pdu on")
                try:
                    result = pdu_ctrl(params['pdu_ip'], params['pdu_slot'], "on")
                    if result:
                        response["result"]["message"] = "ON"
                    else:
                        response["result"]["message"] = "FAIL"
                except Exception as e:
                    logger.info("Erreur: {}".format(e))
            elif msg.subject.lower() == "call.manage_pdu.off":
                logger.info("handled pdu off")
                try:
                    logger.info("valeur de params : {}".format(params))
                    result = pdu_ctrl(params["pdu_ip"], params["pdu_slot"], "off")
                    if result:
                        response["result"]["message"] = "OFF"
                    else:
                        response["result"]["message"] = "FAIL"
                except Exception as e:
                    logger.info("Erreur: {}".format(e))
            elif msg.subject.lower() == "call.manage_pdu.state":
                logger.info("handled pdu state")
                try:
                    result = pdu_ctrl(params["pdu_ip"], params["pdu_slot"], "state")
                    if result:
                        response["result"]["message"] = "ON"
                    else:
                        response["result"]["message"] = "OFF"
                except Exception as e:
                    logger.info("Erreur: {}".format(e))
            elif msg.subject.lower() == "call.manage_pdu.restart":
                logger.info("handled pdu restart")
                try:
                    result = pdu_ctrl(params["pdu_ip"], params["pdu_slot"], "restart")
                    if result:
                        response["result"]["message"] = "PASS"
                    else:
                        response["result"]["message"] = "FAIL"
                except Exception as e:
                    logger.info("Erreur: {}".format(e))
            elif msg.subject.lower() == "call.manage_pdu.ping":
                logger.info("handled pdu ping")
                try:
                        response["result"]["message"] = "pong"
                except Exception as e:
                    logger.info("Erreur: {}".format(e))
            elif msg.subject.lower() == "call.manage_pdu.options":
                logger.info("handled pdu options")
                try:
                        response["result"]["message"] = newmsg
                except Exception as e:
                    logger.info("Erreur: {}".format(e))
            else:
                logger.info("unhandled command: {subj}".format(subj=msg.subject))
                response["result"]["errors"] = "Commande inconnue"
        except:
            response['result']['message'] = "Erreur de syntaxe (pdu_ip, pdu_slot)"

        await nc.publish(msg.reply, json.dumps(response).encode())

    if nc.is_connected:
        logger.info("Connection established")
        await nc.subscribe("access.manage_pdu", cb=access_handler)  # definir les methodes dispo
        await nc.subscribe("call.manage_pdu.*", cb=pdu_handler)
        logger.info("Handlers has been registered")

    if nc.is_connecting:
        logger.info("Connection in progress")

    if nc.is_closed:
        logger.info("Connection closed")

    if nc.is_reconnecting:
        logger.info("Trying to reconnect")

 #On boucle a l'infini
    while True: #nbm <= nbmessages:
        await nc.flush(1)
if __name__ == '__main__':
    try:
#        loop.create_task(run(loop))
#        loop.run_forever()
         loop = asyncio.get_event_loop()
         loop.run_until_complete(run(loop))
         loop.close()
    except KeyboardInterrupt:
        pass
