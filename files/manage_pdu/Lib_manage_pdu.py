# -*- coding: utf-8 -*-
import requests
import json
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-r","--resgate", help="--resgate http://127.0.0.1:8080")
parser.add_argument("-i","--pdu_ip", help="--pdu_ip 192.168.100.55")
parser.add_argument("-s","--pdu_slot", help="--pdu_slot  1|2|3|4")
parser.add_argument("-a","--pdu_action", help="--pdu_action  state|on|off")

args = parser.parse_args()
if args.resgate:
    RESGATE=args.resgate
else:
    RESGATE="http://10.194.67.234:8080"

if args.pdu_ip:
    pdu_ip=args.pdu_ip
else:
    pdu_ip="192.168.100.55"

if args.pdu_slot:
    pdu_slot=args.pdu_slot
else:
    pdu_slot="1"

if args.pdu_action:
    pdu_action=args.pdu_action
else:
    pdu_action="state"

def manage_pdu(url, pdu_ip, pdu_slot, pdu_action):
        uri = "/api/manage_pdu/{}".format(pdu_action)
        data = {'pdu_ip': pdu_ip, 'pdu_slot':pdu_slot,'timeout': 60000}
        s = requests.session()
        r = s.post(url+uri,data=json.dumps(data), timeout=600)
        #       print(r.status_code)
        print(r.json())
        if r.status_code == 200:
                return "PASS"
        else:
                return "FAIL"
        #+ "=>" + r.json()


# test de la fonction table
if __name__ == "__main__":
        print("Status du pdu " + str(manage_pdu(RESGATE, pdu_ip, pdu_slot, pdu_action)))
