import re
import telnetlib
import time


""" Settings for PDUs """
pdu_admin_username = 'admin'
pdu_admin_password = '12345678'
default_timeout = 30
default_reboot_waiting_time_secs = 5
default_debug_level = 1


def pdu_ctrl(ipaddr, slot, command):
    returns = False
    try:
        with telnetlib.Telnet(ipaddr) as tn:
            tn.set_debuglevel(default_debug_level)
            tn.read_until(b"IP_POWER login: ")
            tn.write(pdu_admin_username.encode('ascii') + b"\r\n")
            print("Sending login")
            tn.read_until(b"Password: ")
            tn.write(pdu_admin_password.encode('ascii') + b"\r\n")
            print("Sending password")
            tn.read_until(b'#')
            print("Beginning telnet transaction")
            if command == 'on':
                print("Sending power on to {} slot {}".format(ipaddr, slot))
                tn.write(b"ioctrl --setpower p6" + slot.encode('ascii') + b"=1\r\n")
                try:
                    tn.read_until(b"ioctrl --setpower p6" + slot.encode('ascii') + b"=1")
                    print("Command received by PDU")
                    try:
                        tn.read_until(b"p6" + slot.encode('ascii') + b" ok")
                        print("Command accepted")
                        returns = True
                        print("Power on sent")
                    except EOFError as e:
                        print(e)
                except EOFError as e:
                    print(e)
            elif command == 'off':
                print("Sending power off to {} slot {}".format(ipaddr, slot))
                tn.write(b"ioctrl --setpower p6" + slot.encode('ascii') + b"=0\r\n")
                try:
                    tn.read_until(b"ioctrl --setpower p6" + slot.encode('ascii') + b"=0")
                    print("Command received by PDU")
                    try:
                        tn.read_until(b"p6" + slot.encode('ascii') + b" ok")
                        print("Command accepted")
                        returns = True
                        print("Power off sent")
                    except EOFError as e:
                        print(e)
                except EOFError as e:
                    print(e)
            elif command == 'state':
                print("Getting state from {} slot {}".format(ipaddr, slot))
                tn.write(b"ioctrl --getpower\r\n")
                try:
                    tn.read_until(b"ioctrl --getpower")
                    print("Command received by PDU")
                    try:
                        exc = tn.expect([b"(\d, \d, \d, \d)"])[1].group(1).decode().split(",")
                        if int(exc[int(slot)-1]) == 1:
                            returns = True
                        print("Power state sent")
                    except EOFError as e:
                        print(e)
                except EOFError as e:
                    print(e)
            elif command == 'restart':
                print("Restarting PDU")
                tn.write(b"ioctrl --setpower p6" + slot.encode('ascii') + b"=0\r\n")
                try:
                    tn.read_until(b"ioctrl --setpower p6" + slot.encode('ascii') + b"=0")
                    print("PDU has been shutted down, waiting {} secs before turning on".format(default_reboot_waiting_time_secs))
                    try:
                        tn.read_until(b"p6" + slot.encode('ascii') + b" ok")
                        time.sleep(default_reboot_waiting_time_secs)
                        tn.write(b"ioctrl --setpower p6" + slot.encode('ascii') + b"=1\r\n")
                        print("Turning on")
                        try:
                            tn.read_until(b"ioctrl --setpower p6" + slot.encode('ascii') + b"=1")
                            try:
                                tn.read_until(b"p6" + slot.encode('ascii') + b" ok")
                                print("Reboot finished successful")
                                returns = True
                            except EOFError as e:
                                print(e)
                        except EOFError as e:
                            print(e)
                    except EOFError as e:
                        print(e)
                except EOFError as e:
                    print(e)
            else:
                print("Commande inconnue: {}".format(command))
            # result = tn.read_all()
            tn.write(b"exit\n")
            print(returns)
            tn.close()
    except Exception as e:
        print("Erreur lors de la connexion telnet a l'equipement {}: {}".format(ipaddr, e))
    return returns
if __name__ == '__main__':
    pdu_ctrl("192.168.100.55", "1" , "state" )
#    pdu_ctrl("192.168.100.55", "1" , "state" )
