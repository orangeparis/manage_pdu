# -*- coding: utf-8 -*-
import json
import logging
import os
import sys

# création de l'objet logger qui va nous servir à écrire dans les logs
logger = logging.getLogger()
# on met le niveau du logger à DEBUG, comme ça il écrit tout
logger.setLevel(logging.INFO)

# création d'un formateur qui va ajouter le temps, le niveau
# de chaque message quand on écrira un message dans le log
formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(filename)s  :: %(message)s')
# création d'un second handler qui va rediriger chaque écriture de log
# sur la console
stream_handler = logging.StreamHandler(sys.stdout)
stream_handler.setFormatter(formatter)
stream_handler.setLevel(logging.DEBUG)
logger.addHandler(stream_handler)
class gitoun:
    def __init__(self):
        self.gitname = "basebox"
        #self.url = "https://www.forge.orange-labs.fr/plugins/git/bebacces/{}.git".format(self.gitname)
        self.url = "https://philippecurci@bitbucket.org/orangeparis/{}.git".format(self.gitname)
        self.home = "/app"
        self.path = "{}/{}".format(self.home,self.gitname)
        logger.info("Repository cloning : {}".format(self.path))
        os.system('echo aHR0cHM6Ly9waGlsaXBwZWN1cmNpOlNneHg3MzgyLUBiaXRidWNrZXQub3JnCg== | base64 -d > ~/.git-credentials')

    def clone(self):
        logger.info("Repository cloning : {}".format(self.path))
        os.system("git clone {}".format(self.url))
        logger.info("Repository configuration without enter login/password")
        os.system("cd {} ; git config credential.helper store".format(self.path))

    def pull(self):
        logger.info("Repository pull : {}".format(self.path))
        os.system("cd {} ; git pull".format(self.path))

    def commit(self):
        try:
            logger.info("Repository add : {}".format(self.path))
            os.system("cd {} ; git add *".format(self.path))
            logger.info("Repository commit")
            os.system('cd {} ; git commit -m "OK"'.format(self.path))
            logger.info("Repository push")
            os.system("cd {} ; git push".format(self.path))
        except:
            logger.info("Erreur lors du git push")


def access_response(methodes):
    """
    """
    # print("access_response()")
    # #smethodes = ",".join(*methodes)
    content = {"result": {"get": True}}
    content["result"]["call"] = methodes
    # print(content)
    # content={"result" : {"get": True, "call":"set,find"}}
    return content


def response(data):
    """
    """
    return {"result": data}


def error_response(code, message):
    """
    """
    return {"error": {"code": code, "message": message}}


def parameters(msg):
    """
    le parametre msg est un message de type NATS.msg
    """
    #    a = {}
    #    a = json.loads(msg.data.decode())
    #    print(a['params'] )
    #    b=a['params']
    #    #return b
    return json.loads(msg.data.decode())['params']
