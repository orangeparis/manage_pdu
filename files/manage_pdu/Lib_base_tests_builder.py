# -*- coding: utf-8 -*-
import requests
import json
import argparse
import Lib_manage_pdu as m #import manage_pdu

parser = argparse.ArgumentParser()
parser.add_argument("-r","--resgate", help="--resgate http://127.0.0.1:8080")
parser.add_argument("-i","--pdu_ip", help="--pdu_ip 192.168.100.55")
parser.add_argument("-s","--pdu_slot", help="--pdu_slot  1|2|3|4")
parser.add_argument("-a","--pdu_action", help="--pdu_action  state|on|off")

args = parser.parse_args()
if args.resgate:
    RESGATE=args.resgate
else:
    RESGATE="http://10.194.67.234:8080"

if args.pdu_ip:
    pdu_ip=args.pdu_ip
else:
    pdu_ip="192.168.100.55"

if args.pdu_slot:
    pdu_slot=args.pdu_slot
else:
    pdu_slot="1"

if args.pdu_action:
    pdu_action=args.pdu_action
else:
    pdu_action="state"
def generate_template_pdu_on_off(template_name,lb_name, pdu_name,pdu_ip,pdu_slot):

    if len(lb_name) > 0 and len(pdu_name) > 0 and len(pdu_slot) > 0 and len(pdu_ip) > 0:
        robot = open("./robots/{}_{}_{}_OFF_ON.robot".format(lb_name,pdu_name,pdu_slot),'w')
        with open(template_name, 'r') as myTemplate:
            data = myTemplate.read()
            line=data.replace("lb_name",lb_name)
            line=line.replace("pdu_name",pdu_name)
            line=line.replace("pdu_ip",pdu_ip)
            line=line.replace("pdu_slot",pdu_slot)
            robot.write(line)
    
            #print('data.replace("lb_name",{}'.format(lb_name))
            # print template for visual cue.
            print('Template passed:\n' + '-'*30 +'\n' + line)
            print('-'*30)
        robot.close()
def lb_retrieve(url, pdu_ip, pdu_slot, pdu_action):
    uri = "/api/inventory/livebox/query"
    data = {'lb_benche': "M3"} #,'timeout': 60000}
    s = requests.session()
    r = s.post(url+uri,data=json.dumps(data), timeout=600)
    #       print(r.status_code)
    #print(r.json())
    for box in r.json():
        print("{} {} {} {}".format(box['name'],box['pdu_name'], box['pdu_ip'],box['pdu_socket']))
        m.manage_pdu(url, box['pdu_ip'], box['pdu_socket'],"state")  
        generate_template_pdu_on_off("./manage_pdu.robot.template",box['name'],box['pdu_name'], box['pdu_ip'],box['pdu_socket'])
    if r.status_code == 200:
        return "PASS"
    else:
        return "FAIL"
        #+ "=>" + r.json()


# test de la fonction table
if __name__ == "__main__":
    print("Status du pdu " + str(lb_retrieve(RESGATE, pdu_ip, pdu_slot, pdu_action)))
