job "managepdu" {
  datacenters = ["dc1"]

  group "pdu" {
    count = 1
    restart {
      attempts = 10
      interval = "5m"
      delay = "25s"
      mode = "delay"
    }
  task "managepdu" {
     driver = "docker"
  config {
     image = "manage_pdu:local"
     args =  ["--nats", "nats://10.194.67.234:4222",]
     }  
   }
      service {
        name = "managepdu"
        tags = ["managepdu"]

        check {
          type     = "script"
          name     = "nping_manage_pdu"
          command  = "/usr/bin/labo"
          args     = ["ping","--nats","nats://10.194.67.234:4222","--service","manage_pdu"]
          interval = "60s"
          timeout  = "1s"

          check_restart {
            limit = 3
            grace = "10s"
            ignore_warnings = false
          }
        }
      }
  }
}
