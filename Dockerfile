FROM python:alpine


ENV HTTP_PROXY=http://10.194.67.201:3128
ENV https_proxy=http://10.194.67.201:3128
ENV http_proxy=http://10.194.67.201:3128
ENV no_proxy=localhost,10.194.67.201,localaddress,.localdomain.com
ENV NO_PROXY=10.194.67.*,localhost,10.194.67.201,localaddress,.localdomain.com,192.168.*
ENV HTTPS_PROXY=http://10.194.67.201:3128


# install packages
RUN apk update && apk add git wget ca-certificates && update-ca-certificates


WORKDIR /app


# install application
COPY files/ .
RUN pip install -r requirements.txt

RUN echo "cGhpbGlwcGUuY3VyY2lAb3JhbmdlLmNvbQo=" | base64 -d | xargs git config --global user.email 
RUN echo "UGhpbGlwcGUK" | base64 -d | xargs git config --global user.name 

RUN echo "aHR0cHM6Ly9jdnZxNjQwMzo1LVNneHg3MzgyQHd3dy5mb3JnZS5vcmFuZ2UtbGFicy5mcgo=" | base64 -d > ~/.git-credentials
RUN mv .gitconfig $HOME/
#RUN mv labo /usr/bin/
#RUN chmod +x /usr/bin/labo
#CMD sh

ENTRYPOINT [ "python" ,"app.py"]







